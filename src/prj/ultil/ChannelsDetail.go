package ultil

import (
	"fmt"
	"sync"

	"github.com/fatih/color"
)

func InitChannel() {
	// fmt.Printf("chan<- int 'chan chi gui ko, chan 1 chieu' sendData(sendch chan<- int)  chan2 := make(chan int),  ")
	// giao tiep giua cac goroutines // ko can dung bien ngoai
	// unbuffered  phai chuyen data vao chan va bat buoc phai nhan != -> block
	// buffered cho phep dang ki so luong vao chan
	// select su dung trong nhieu go for { select { case v:= <-q }}
	color.Yellow("Concurrency is about dealing with lots of things at once.")
	// var listOfTasks = []func(*sync.WaitGroup){
	// 	makeHotelReservation, bookFlightTickets, orderADress,
	// 	payCreditCardBills, writeAMail, listenToAudioBook,
	// }
	// waitGroup.Add(len(listOfTasks))
	// for _, task := range listOfTasks {
	// 	go task(&waitGroup)
	// }
	// waitGroup.Wait()
	var wg2 sync.WaitGroup
	wg2.Add(30)
	ordersChannel := make(chan int)
	for i := 0; i < 3; i++ {
		// Start the three cashiers
		func(i int) {
			go cashier(i, ordersChannel, &wg2)
		}(i)
	}
	// Start adding orders to be processed.
	for i := 0; i < 30; i++ {
		ordersChannel <- i
	}
	wg2.Wait()
}

func cashier(cashierID int, orderChannel <-chan int, wg2 *sync.WaitGroup) {
	// Process orders upto limit.
	for ordersProcessed := 0; ordersProcessed < 10; ordersProcessed++ {
		// Retrieve order from orderChannel
		orderNum := <-orderChannel
		// Cashier is ready to serve!
		fmt.Println("[Cashier]", cashierID, "- Processing order", orderNum)
		wg2.Done()
	}
}
