package domain

import (
	"net/http"
)

// actOn{GET, POST, DELETE, PUT} functions return Response based on specific Request type.
func actOnGET(listFpt map[string]Fpt, act Action) {
	// These initialized values cover the case:
	// Request asked for an id that doesn't exist.
	status := http.StatusNotFound
	FptResult := []Fpt{}
	if act.Id == "" {
		// Request asked for all fpts.
		status = http.StatusOK
		for _, fpt := range listFpt {
			FptResult = append(FptResult, fpt)
		}
	} else if fpt, exists := listFpt[act.Id]; exists {
		// Request asked for a specific fpt and the id exists.
		status = http.StatusOK
		FptResult = []Fpt{fpt}
	}
	act.RetChan <- Response{
		StatusCode: status,
		ListsFpt:   FptResult,
	}
}

func actOnDELETE(listFpt map[string]Fpt, act Action) {
	fpt, exists := listFpt[act.Id]
	delete(listFpt, act.Id)
	if !exists {
		fpt = Fpt{}
	}
	// Return the deleted fpt if it exists else return an empty fpt.
	act.RetChan <- Response{
		StatusCode: http.StatusOK,
		ListsFpt:   []Fpt{fpt},
	}
}
func actOnPUT(listFpt map[string]Fpt, act Action) {
	// These initialized values cover the case:
	// Request asked for an id that doesn't exist.
	status := http.StatusNotFound
	fptResult := []Fpt{}
	// If the id exists, update its values with the values from the payload.
	if fpt, exists := listFpt[act.Id]; exists {
		fpt.Name = act.Payload.Name
		fpt.App = act.Payload.App
		listFpt[act.Id] = fpt
		status = http.StatusOK
		fptResult = []Fpt{listFpt[act.Id]}
	}
	// Return status and updated resource.
	act.RetChan <- Response{
		StatusCode: status,
		ListsFpt:   fptResult,
	}
}
func actOnPOST(listFpt map[string]Fpt, act Action, newID string) {
	// Add the new Fpt to 'Fpts'.
	listFpt[newID] = Fpt{
		Id:   newID,
		Name: act.Payload.Name,
		App:  act.Payload.App,
	}
	act.RetChan <- Response{
		StatusCode: http.StatusCreated,
		ListsFpt:   []Fpt{listFpt[newID]},
	}
}
