package controller

import (
	"fmt"
	"log"
	"net/http"
	"prj/domain"
)

// Aplication FPT
// MongoTemplate

func InitFpt() {
	listFpt := GetListFpt()
	actionCh := make(chan domain.Action)
	go domain.StartFptsManager(listFpt, actionCh)
	http.HandleFunc("/api/list_fpt/", MakeHandler(domain.FPTHandler, "/api/list_fpt/", actionCh))
}

func GetListFpt() map[string]domain.Fpt {
	list := map[string]domain.Fpt{}
	for index := 1; index < 10; index++ {
		id := fmt.Sprintf("%d", index)
		list[id] = domain.Fpt{
			Id:   id,
			App:  fmt.Sprintf("APPF%s", id),
			Name: fmt.Sprintf("Name APPF %s", id),
		}
	}
	return list
}

func MakeHandler(fn func(http.ResponseWriter, *http.Request, string, string, chan<- domain.Action), endpoint string, actionCh chan<- domain.Action) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		method := r.Method
		msg := fmt.Sprintf("Received request [%s] for path: [%s]", method, path)
		log.Println(msg)
		id := path[len(endpoint):]
		log.Println("Call .....", r.URL.Path)
		fn(w, r, id, method, actionCh)
	}
}
