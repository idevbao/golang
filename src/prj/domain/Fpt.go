package domain

// Fpt is ...
type Fpt struct {
	Id   string `json:"id"`
	App  string `json:"app"`
	Name string `json:"name"`
}

// RequestPayload is ...
type RequestPayload struct {
	App  string `json:"app"`
	Name string `json:"name"`
}

type Response struct {
	StatusCode int
	ListsFpt   []Fpt
}

type Action struct {
	Id      string
	Type    string
	Payload RequestPayload
	RetChan chan<- Response
}

type DowImg interface {
	dow()
}

type CallApi interface {
	call()
}

type Action2 interface {
	DowImg
	CallApi
}

func (fpt Fpt) dow() {

}

func (fpt Fpt) call() {

}

func (fpt Fpt) initFpt() {

}

// func init() {
// fpt := Fpt{"1", "app", "name"}
// fmt.Println("init obj FPT")
// var interfaceNew Action2 = fpt
// interfaceNew.dow()
// interfaceNew.call()
// }

// func emtyInterface(i interface{}) {
// fmt.Println("?", i)
// Println func(a ...interface{}) (n int, err error)
// }
