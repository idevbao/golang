package typeGo

import (
	"fmt"

	"github.com/fatih/color"
)

func init() {
	// fmt.Printf("make(map[type of key]type of value)   ")
}

func ExpMapGO() {
	personSalary := make(map[string]int)
	fmt.Println(personSalary == nil)
	var personSalary1 map[string]int
	fmt.Println(personSalary1 == nil)
	color.Red("Can i have color red ?  Yes! You can")
}
