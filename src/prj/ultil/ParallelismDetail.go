package ultil

import (
	"fmt"
	"sync"
	"time"

	"github.com/fatih/color"
)

var wg sync.WaitGroup

func InitParallelism() {
	// thuc thi nhieu task trong cung 1 thoi diem nhung cpu pahi co tu 2 core tro len

	color.Yellow("Parallelism: Parallelism is about doing lots of things at once.")
	var waitGroup1 sync.WaitGroup
	defer timeTrack(time.Now())
	go listenForever()
	waitGroup1.Add(3)

	// Give some time for listenForever to start
	time.Sleep(time.Nanosecond * 10)
	// Let's start writing the mails
	go writeMail1(&waitGroup1)
	go writeMail2(&waitGroup1)
	go writeMail3(&waitGroup1)
	waitGroup1.Wait()
}

func listenForever() {
	for index := 0; index < 10; index++ {
		printTime("Listening...")
	}
}
func printTime(msg string) {
	fmt.Println(msg, time.Now().Format("15:04:05"))
}

// Task that will be done over time
func writeMail1(wg *sync.WaitGroup) {
	printTime("Done writing mail #1.")
	wg.Done()
}
func writeMail2(wg *sync.WaitGroup) {
	printTime("Done writing mail #2.")
	wg.Done()
}
func writeMail3(wg *sync.WaitGroup) {
	printTime("Done writing mail #3.")
	wg.Done()
}

func timeTrack(start time.Time) {
	done := time.Since(start)
	fmt.Printf("\nDone Concurrent  %v", done)
}
