package main

import (
	"net/http"
	"prj/controller"
)

func main() {
	// runtime.GOMAXPROCS(6)
	// var initP = runtime.GOMAXPROCS(0)
	// runtime.NumCPU()
	// fmt.Println(initP)
	// go ultil.InitChannel()
	// go ultil.InitParallelism()
	// time.Sleep(time.Second * 10)
	// fmt.Println(runtime.NumGoroutine())
	controller.InitLogin()
	controller.InitFpt()
	http.ListenAndServe(":9999", nil)
}
