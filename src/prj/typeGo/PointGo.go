package typeGo

import "fmt"

func change(val *int) {
	*val = 55
}

func init() {
	a := 58
	fmt.Println("value of a before function call is", a)
	b := &a // var b *int = &a   point to address b
	fmt.Printf("Type of a is %T\n", b)
	fmt.Println("value of b after function call is", *b, b)
	change(b)
	fmt.Println("value of a after function call is", a)

	sls := []int{}
	fmt.Println("cars:", sls, "has old length", len(sls), "and capacity", cap(sls))
	sls = append(sls, 1, 2)
	fmt.Println("cars:", sls, "has old length", len(sls), "and capacity", cap(sls))
	usingPointSliceForArr(sls)
	fmt.Println("value of b after function call is", sls)
}

func usingPointSliceForArr(sls []int) {
	sls[0] = 10
}
