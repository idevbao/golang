package domain

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// StartBooksManager starts a goroutine that changes the state of books (map).
// Primary reason to use a goroutine instead of directly manipulating the books map is to ensure
// that we do not have multiple requests changing books' state simultaneously.
func StartFptsManager(listFpt map[string]Fpt, actionCh <-chan Action) {
	newID := len(listFpt)
	for {
		select {
		case act := <-actionCh:
			switch act.Type {
			case "GET":
				actOnGET(listFpt, act)
			case "POST":
				newID++
				newBookID := fmt.Sprintf("%d", newID)
				actOnPOST(listFpt, act, newBookID)
			case "PUT":
				actOnPUT(listFpt, act)
			case "DELETE":
				actOnDELETE(listFpt, act)
			}
		}
	}
}

/* Handler is responsible for ensuring that we process only the valid HTTP Requests.
* GET -> id: Any
* POST -> id: No
* -> payload: Required
* PUT -> id: Any
* -> payload: Required
* DELETE -> id: Any
 */
func FPTHandler(w http.ResponseWriter, r *http.Request, id string, method string, actionCh chan<- Action) {
	// Ensure that id is set only for valid requests
	isGet := method == "GET"
	idIsSetForPost := method == "POST" && id != ""
	isPutOrPost := method == "PUT" || method == "POST"
	idIsSetForDelPut := (method == "DELETE" || method == "PUT") && id != ""
	if !isGet && !(idIsSetForPost || idIsSetForDelPut || isPutOrPost) {
		writeError(w, http.StatusMethodNotAllowed)
		return
	}
	respCh := make(chan Response)
	act := Action{
		Id:      id,
		Type:    method,
		RetChan: respCh,
	}
	// PUT & POST require a properly formed JSON payload
	if isPutOrPost {
		var reqPayload RequestPayload
		body, _ := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err := json.Unmarshal(body, &reqPayload); err != nil {
			writeError(w, http.StatusBadRequest)
			fmt.Println("Error Handler Method P")
			return
		}
		fmt.Println("Successful", reqPayload)
		act.Payload = reqPayload
	}
	// We have all the data required to process the Request.
	// Time to update the state of fpts.
	actionCh <- act
	// Wait for respCh to return data after updating the state of fpts.
	// For all successful Actions, the HTTP status code will either be 200 or 201.
	// Any other status code means that there was an issue with the request.
	var resp Response
	if resp = <-respCh; resp.StatusCode > http.StatusCreated {
		// writeError(w, resp.StatusCode)
		return
	}
	// We should only log the delete resource and not send it back to user
	if method == "DELETE" {
		log.Println(fmt.Sprintf("Resource ID %s deleted: %+v", id, resp.ListsFpt))
		resp = Response{
			StatusCode: http.StatusOK,
			ListsFpt:   []Fpt{},
		}
	}
	writeResponse(w, resp)
}

func writeResponse(w http.ResponseWriter, resp Response) {
	var err error
	var serializedPayload []byte
	if len(resp.ListsFpt) == 1 {
		serializedPayload, err = json.Marshal(resp.ListsFpt[0])
	} else {
		serializedPayload, err = json.Marshal(resp.ListsFpt)
	}
	if err != nil {
		writeError(w, http.StatusInternalServerError)
		fmt.Println("Error while serializing payload: ", err)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(resp.StatusCode)
		w.Write(serializedPayload)
	}
}

func writeError(w http.ResponseWriter, statusCode int) {
	jsonMsg := struct {
		Msg  string `json:"msg"`
		Code int    `json:"code"`
	}{
		Code: statusCode,
		Msg:  http.StatusText(statusCode),
	}
	if serializedPayload, err := json.Marshal(jsonMsg); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		fmt.Println("Error while serializing payload: ", err)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(serializedPayload)
	}
}
